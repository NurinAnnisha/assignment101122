package HighestPlacement;

import java.util.Scanner;

public class Version1 {
	
	int pCSE, pECE, pMECH; 
	
	public static void main(String[] args) {
	
	Scanner scan = new Scanner(System.in); 
	
	System.out.println("Enter placement for CSE");
	int pCSE = scan.nextInt(); 
	
	System.out.println("Enter placement for ECE");
	int pECE = scan.nextInt();
	
	System.out.println("Enter placement for MECH");
	int pMECH = scan.nextInt();
	
	if ((pCSE >= 0) && (pECE >= 0) && (pMECH >=0)) { //if one of the inputs is negative, automatically cannot enter this condition
		if (pCSE > pECE && pCSE > pMECH){
			System.out.println("Highest placement");
			System.out.println("CSE");
		} else if (pECE > pCSE && pECE > pMECH) {
			System.out.println("Highest placement");
			System.out.println("ECE");
		} else if (pMECH > pCSE && pMECH > pECE) {
			System.out.println("Highest placement");
			System.out.println("MECH");
		} else if (pCSE == 0 || pECE == 0 || pMECH == 0) {
			System.out.println("None of the department has got the highest placement");
		}  else if ((pCSE == pECE) && (pCSE == pMECH) && (pECE == pMECH)){
			System.out.println("None of the department has got the highest placement");
		} else if ((pCSE == pECE) && (pCSE > pMECH) && (pECE > pMECH)) {
			System.out.println("Highest placement");
			System.out.println("CSE");
			System.out.println("ECE");
		} else if ((pCSE == pMECH) && (pCSE > pECE) && (pMECH > pECE)) {
		  	System.out.println("Highest placement");
		  	System.out.println("CSE");
		  	System.out.println("MECH");
		} else if ((pECE == pMECH) && (pECE > pCSE) && (pMECH > pCSE)) {
		  	System.out.println("Highest placement");
		  	System.out.println("ECE");
		  	System.out.println("MECH");
		}
	} else {
		System.out.println("Input is invalid");
	} 
	
 }

}