package MultiplexTheaterExercise;

import java.util.Scanner;

public class Version1 {
	
	int numOfTicket;
	char refreshment, couponCode, typeOfCircle;
	static double totalCost, refreshmentCost, totalRefreshmentCost, couponCodeDiscount, totalCouponCodeDiscount, extraDiscount; 

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in); 
		
		System.out.println("Enter the no of ticket: ");
		int numOfTicket= scan.nextInt(); 
		
		if (numOfTicket>= 5 && numOfTicket<=40) {
			
			if (numOfTicket>=20 && numOfTicket<=40) {
				
				extraDiscount = 0.90; 
				
				System.out.println("Do you want refreshment? Enter Y if yes, N if no: ");
				char refreshment = scan.next().charAt(0); 
				
				if(refreshment == 'Y') {
				    refreshmentCost = 50; 
				    totalRefreshmentCost = refreshmentCost*numOfTicket; 
				} else if  (refreshment == 'N') { 
					totalRefreshmentCost = 0; 	
				} 
				
			   System.out.println("Do you have coupon? Enter Y if yes, N if no: ");
			   char couponCode = scan.next().charAt(0); 
			   
			   if(couponCode == 'Y') {
				    couponCodeDiscount = 0.98; 
				    totalCouponCodeDiscount = 0.98; 
			   } else if  (couponCode == 'N') { 
					totalCouponCodeDiscount = 0; 	
			   }   	   
		 } else {
					System.out.println("Do you want refreshment? Enter Y if yes, N if no: ");
					char refreshment = scan.next().charAt(0); 
				
					if(refreshment == 'Y') {
						refreshmentCost = 50; 
						totalRefreshmentCost = refreshmentCost*numOfTicket; 
					} else if  (refreshment == 'N') { 
						totalRefreshmentCost = 0; 	
					}
		 	 }
			
		 System.out.println("Enter the circle. Enter K if king, Q if queen");
		 char typeOfCircle = scan.next().charAt(0);
		 
		 if (typeOfCircle == 'K') {
			 totalCost = ((75*numOfTicket)*(extraDiscount*totalCouponCodeDiscount))+totalRefreshmentCost; 
			 double roundOff = Math.round(totalCost*100)/100;
			 System.out.println("Ticket cost: " +totalCost);
		 }  else if (typeOfCircle == 'Q') {
			 totalCost = ((150*numOfTicket)*(extraDiscount*totalCouponCodeDiscount))+totalRefreshmentCost; 
			 double roundOff = Math.round(totalCost*100)/100;
			 System.out.println("Ticket cost: " +totalCost);
		 } else {
			 System.out.println("Invalid input");
		 }
		 
		} else {
		  System.out.println("Minimum of 5 and Maximum of 40 Tickets");
		}
	}

}
	
